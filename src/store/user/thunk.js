import { $authHost } from '../../http';
import { loginUserData } from './actionCreators';

export const getUsersMe = () => {
	return async (dispatch) => {
		try {
			const { data } = await $authHost.get('/users/me');
			const { name, email, role } = data.result;
			dispatch(
				loginUserData({
					name,
					email,
					role,
					token: localStorage.getItem('token'),
					isAuth: true,
				})
			);
		} catch (e) {
			console.log(e?.response?.statusText);
			localStorage.removeItem('token');
		}
	};
};
