import {
	ADD_ALL_COURSES,
	CREATE_COURSE,
	DELETE_COURSE,
	UPDATE_COURSE,
} from './actionTypes';

export const addAllCoursesData = (payload) => ({
	type: ADD_ALL_COURSES,
	payload,
});
export const deleteCourseData = (payload) => ({ type: DELETE_COURSE, payload });
export const createCourseData = (payload) => ({ type: CREATE_COURSE, payload });
export const updateCourseData = (payload) => ({ type: UPDATE_COURSE, payload });
