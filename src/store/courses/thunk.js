import {
	addAllCoursesData,
	createCourseData,
	deleteCourseData,
	updateCourseData,
} from './actionCreators';
import { $authHost } from '../../http';

export const getAllCourses = () => {
	return async (dispatch) => {
		try {
			const { data } = await $authHost.get('/courses/all');
			dispatch(addAllCoursesData(data.result));
		} catch (e) {
			console.log(e?.message);
		}
	};
};

export const deleteCourse = (id) => {
	return async (dispatch) => {
		try {
			const { data } = await $authHost.delete(`/courses/${id}`);
			if (data?.successful) {
				dispatch(deleteCourseData(id));
			}
		} catch (e) {
			console.log(e?.message);
		}
	};
};

export const addCourse = (newCourse) => {
	return async (dispatch) => {
		try {
			const { data } = await $authHost.post('/courses/add', newCourse);
			dispatch(createCourseData(data.result));
		} catch (e) {
			console.log(e?.message);
		}
	};
};

export const updateCourse = (id, updateCourse) => {
	return async (dispatch) => {
		try {
			const { data } = await $authHost.put(`/courses/${id}`, updateCourse);
			dispatch(updateCourseData(data.result));
		} catch (e) {
			console.log(e?.message);
		}
	};
};
