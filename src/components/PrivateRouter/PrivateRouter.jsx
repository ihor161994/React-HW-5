import React from 'react';
import { useSelector } from 'react-redux';
import { getUser } from '../../store/selectors';
import { Navigate } from 'react-router-dom';

const PrivateRouter = ({ children }) => {
	const user = useSelector(getUser);

	return user.role === 'admin' ? children : <Navigate to={'/courses'} />;
};

export default PrivateRouter;
