import { $authHost, $host } from './index';
import { loginUserData } from '../store/user/actionCreators';
import { getUsersMe } from '../store/user/thunk';

export const registration = async (newUser) => {
	const { data } = await $host.post('/register', newUser);
	return data;
};

/*export const login = async (user) => {
	const { data } = await $host.post('/login', user);
	/!*localStorage.setItem('token', data.result);*!/
	return data;
};*/
export const login = (user) => {
	return async (dispatch) => {
		try {
			const { data } = await $host.post('/login', user);
			dispatch(loginUserData(data.result));
			dispatch(getUsersMe());
		} catch (e) {
			console.log(e?.response?.data?.result);
		}
	};
};

export const logout = async () => {
	const response = await $authHost.delete('/logout');
	/*localStorage.removeItem('token');*/
	return response;
};

/*export const getAllCourses = async () => {
	const { data } = await $authHost.get('/courses/all');
	return data;
};*/

/*export const deleteCourse = async (id) => {
	const { data } = await $authHost.get(`/courses/${id}`);
	return data;
};*/

/*export const getAllAuthors = async () => {
	const { data } = await $authHost.get('/authors/all');
	return data;
};*/

/*export const getUsersMe = async () => {
	const { data } = await $authHost.get('/users/me');
	return data;
};*/
