import Header from './components/Header/Header';
import './App.css';
import Courses from './components/Courses/Courses';
import {
	BrowserRouter as Router,
	Navigate,
	Route,
	Routes,
} from 'react-router-dom';
import CourseForm from './components/CourseForm/CourseForm';

import { useEffect } from 'react';

import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';

import { useDispatch, useSelector } from 'react-redux';
import { getUser } from './store/selectors';
import PrivateRouter from './components/PrivateRouter/PrivateRouter';
import { getAllCourses } from './store/courses/thunk';
import { getAllAuthors } from './store/authors/thunk';
import { getUsersMe } from './store/user/thunk';

function App() {
	const dispatch = useDispatch();
	const user = useSelector(getUser);

	useEffect(() => {
		if (localStorage.getItem('token')) {
			dispatch(getUsersMe());
		}
		dispatch(getAllCourses());
		dispatch(getAllAuthors());
	}, []);

	return (
		<div className='App'>
			<Router>
				<Header />

				<Routes>
					{!user.isAuth && (
						<>
							<Route exact path='/registration' element={<Registration />} />
							<Route exact path='/login' element={<Login />} />
						</>
					)}

					<Route exact path='/courses'>
						<Route index element={<Courses />} />
						<Route exact path=':courseId' element={<CourseInfo />} />
						<Route
							exact
							path='add'
							element={
								<PrivateRouter>
									<CourseForm />
								</PrivateRouter>
							}
						/>
						<Route
							exact
							path='update/:courseId'
							element={
								<PrivateRouter>
									<CourseForm />
								</PrivateRouter>
							}
						/>
					</Route>
					<Route
						path='*'
						element={
							user.isAuth ? (
								<Navigate to={'/courses'} />
							) : (
								<Navigate to={'/login'} />
							)
						}
					/>
				</Routes>
			</Router>
		</div>
	);
}

export default App;
